<?php

namespace Drupal\vertical_tabs_config\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure order for this site.
 */
class OrderConfigurationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'vt_order_configuration_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'vertical_tabs_config.order',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('vertical_tabs_config.order');

    $vertical_tabs = vertical_tabs_config_vertical_tab_list(TRUE);

    $form['desc'] = [
      '#type' => 'item',
      '#markup' => $this->t('Reorder vertical tabs in the table to set a global order.'),
    ];

    // https://www.drupal.org/node/1876710
    $form['vertical_tabs_table'] = [
      '#type' => 'table',
      '#header' => [t('Vertical tab'), t('Weight')],
      '#empty' => t('There are no vertical tabs.'),
      // Insert or not selection checkbox on first column.
      '#tableselect' => FALSE,
      // TableDrag: callback arguments for drupal_add_tabledrag().
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'vertical_tabs_table-order-weight',
        ],
      ],
    ];

    foreach ($vertical_tabs as $vt_machine_name => $vt_human_name) {
      $id = 'vertical_tabs_config_' . $vt_machine_name;

      // TableDrag: Mark the table row as draggable.
      $form['vertical_tabs_table'][$id]['#attributes']['class'][] = 'draggable';

      // TableDrag: Sort table row according to its existing/configured weight.
      $form['vertical_tabs_table'][$id]['#weight'] = $config->get('vertical_tabs_config_' . $vt_machine_name);

      // Some table columns containing raw markup.
      $form['vertical_tabs_table'][$id]['label'] = [
        '#plain_text' => $vt_human_name,
      ];
      // TableDrag: Weight column element.
      $form['vertical_tabs_table'][$id]['weight'] = [
        '#type' => 'weight',
        '#title' => t('Weight for @title', ['@title' => $vt_human_name]),
        '#title_display' => 'invisible',
        '#default_value' => $config->get('vertical_tabs_config_' . $vt_machine_name),
        // Classify the weight element for #tabledrag.
        '#attributes' => ['class' => ['vertical_tabs_table-order-weight']],
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $vertical_tabs = vertical_tabs_config_vertical_tab_list();

    foreach ($vertical_tabs as $vt_machine_name => $vt_human_name) {
      $new_value = $values['vertical_tabs_table']['vertical_tabs_config_' . $vt_machine_name]['weight'];
      $this->config('vertical_tabs_config.order')
        ->set('vertical_tabs_config_' . $vt_machine_name, $new_value)
        ->save();
    }

    parent::submitForm($form, $form_state);
  }

}
